const name = 'Awesome terrarium'
const temperature = 10


describe('Automation', () => {
    it('should get an automation', async () => {
        const terrarium = await request
            .post('/api/terrarium')
            .send({name})
            .then(r => r.body)
        await request
            .get(`/api/automation/terrarium/${terrarium._id}`)
            .then(res => {
                expect(res).status(200)
                expect(res).to.be.json
                expect(res.body).has.keys('tid', '_id')
                expect(res.body.tid).to.equal(terrarium._id)
            })
    })
    it('should update an automation', async () => {
        const terrarium = await request
            .post('/api/terrarium')
            .send({name})
            .then(r => r.body)
        await request
            .put(`/api/automation/terrarium/${terrarium._id}`)
            .send({temperature})
            .then(res => {
                expect(res).status(200)
                expect(res).to.be.json
                expect(res.body).has.keys('tid', '_id', 'temperature')
                expect(res.body.temperature).to.equal(temperature)
            })
    })
})