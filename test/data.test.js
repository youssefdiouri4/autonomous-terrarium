const name = 'Awesome terrarium'
const temperature = 10
const ambientLight = 1


describe('Data', () => {
    it('should create data', async () => {
        const terrarium = await request
            .post('/api/terrarium')
            .send({name})
            .then(r => r.body)
        await request
            .post('/api/data/')
            .send({timestamp: Date.now(), tid: terrarium._id})
            .then(res => {
                expect(res).status(200)
                expect(res).to.be.json
                expect(res.body).has.keys('tid', '_id', 'timestamp')
                expect(res.body.tid).to.equal(terrarium._id)
            })
    })
    it('should get data', async () => {
        const terrarium = await request
            .post('/api/terrarium')
            .send({name})
            .then(r => r.body)
        const data = await request
            .post('/api/data/')
            .send({timestamp: Date.now(), tid: terrarium._id, ambientLight})
            .then(r => r.body)
        await request
            .get(`/api/data/${data._id}`)
            .then(res => {
                expect(res).status(200)
                expect(res).to.be.json
                expect(res.body).has.keys('tid', '_id', 'timestamp', 'ambientLight')
                expect(res.body.tid).to.equal(terrarium._id)
            })
    })
})