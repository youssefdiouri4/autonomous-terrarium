const mongoose = require('mongoose')
const errors = require('../errors')


const schema = new mongoose.Schema({
    name: {
        type: String,
        required: errors.httpError(400).message
    }
})

module.exports = mongoose.model('Terrarium', schema)