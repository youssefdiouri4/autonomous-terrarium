const mongoose = require('mongoose')
const errors = require('../errors')


const schema = new mongoose.Schema({
    tid: {
        type: mongoose.Schema.Types.ObjectId,
        required: errors.httpError(400).message,
        ref: 'Terrarium'
    },
    fan: {
        type: Boolean,
    },
    pump: {
        type: Boolean,
    },
    light: {
        type: {
            color: {r: Number, g: Number, b: Number},
            on: Boolean,
            intensity: Number
        },
    },
    temperature: {
        type: Number
    },
    ph: {
        type: Number
    }
})

module.exports = mongoose.model('Automation', schema)