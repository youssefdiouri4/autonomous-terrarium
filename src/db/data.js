const mongoose = require('mongoose')
const errors = require('../errors')


const schema = new mongoose.Schema({
    tid: {
        type: mongoose.Schema.Types.ObjectId,
        required: errors.httpError(400).message,
        ref: 'Terrarium'
    },
    timestamp: {
        type: Number,
        required: errors.httpError(400).message
    },
    ambientLight: {
        type: Number
    },
    soilHumidity: {
        type: Number
    },
    airHumidity: {
        type: Number
    },
    temperature: {
        type: Number
    },
    ph: {
        type: Number
    }
})

module.exports = mongoose.model('Data', schema)