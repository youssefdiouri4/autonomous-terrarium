const express = require('express')
const router = express.Router()


router.use('/data', require('./data'))
router.use('/automation', require('./automation'))
router.use('/terrarium', require('./terrarium'))

module.exports = router