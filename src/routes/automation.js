const express = require('express')
const Automation = require('../db/automation')
const router = express.Router()

router.put('/terrarium/:tid', async (req, res) => {
    const options = {upsert: true, new: true, setDefaultsOnInsert: true}
    const query = {tid: req.params.tid}
    const automation = await Automation.findOneAndUpdate(query, req.body, options)
    await automation.save()
    res.json(automation.toObject({versionKey: false}))
})

router.get('/terrarium/:tid', async (req, res) => {
    const options = {upsert: true, new: true, setDefaultsOnInsert: true}
    const query = {tid: req.params.tid}
    const automation = await Automation.findOneAndUpdate(query, req.body, options)
    res.json(automation.toObject({versionKey: false}))
})

module.exports = router