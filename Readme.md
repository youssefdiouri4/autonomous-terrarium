<h1>Terrarium Automatisation API</h1>

<h2>Project based :</h2>
https://framagit.org/makeforartandscience/terrarium


<h2>Required :</h2>

Raspberry Pi

Docker & Docker Compose installed ( you can find a tutorial here : 
https://dev.to/rohansawant/installing-docker-and-docker-compose-on-the-raspberry-pi-in-5-simple-steps-3mgl )


<h2>Setup :</h2>
Start docker compose
In your repository, send the command line

> sudo docker-compose up 

to hide the logs add -d for discreat
``

> sudo docker-compose up -d 


Your server is now up


